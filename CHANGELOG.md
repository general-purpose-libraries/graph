# CHANGELOG

## Version 2.0
* Reworked API (#20)

## Version 1.2
* Added logging (#19)
* Made algorithms run in parallel threads (#18)

## Version 1.1
* Added documentation (#8)
* Added changelog (#14)
* Added readme file (#15)
* Uploaded to maven central repo (#16)

## Version 1.0
* Added more unit tests (#1)
* Added Dijkstra algorithm (#2)
* Added PathStar algorithm (#3)
* Added GraphWalker algorithm (#4)
* Added javadoc (#5)
* Added license (#9)
* Added copyright notice (#12)
* Set up Gitlab CI runner (#13)