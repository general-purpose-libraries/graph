package graph.algorithm;

import graph.Graph;
import graph.edge.Edge;
import graph.exception.edge.EdgeException;
import graph.helperclasses.Path;
import graph.node.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by chief on 20.03.17.
 */
class PathStarTest {
    private Graph<Integer> graph;
    private Graph<Integer> graph2;
    private Graph<Integer> graph3;
    private Graph<Integer> graph4;
    private List<Node<Integer>> nodes;

    @BeforeEach
    void setUp() throws EdgeException {
        nodes = new LinkedList<>();
        for (int i = 0; i < 10; i++){
            Node<Integer> node = mock(Node.class);
            nodes.add(node);
            when(node.getData()).thenReturn(i);
            when(node.toString()).thenReturn(String.valueOf(i));
        }

        List<Edge<Integer>> edges = new ArrayList<>();
        Edge<Integer> edge;
        edge = mock(Edge.class); //0
        when(edge.getTarget(nodes.get(0))).thenReturn(nodes.get(1));
        when(edge.getTarget(nodes.get(1))).thenReturn(nodes.get(0));
        when(edge.getSource(nodes.get(1))).thenReturn(nodes.get(0));
        when(edge.getSource(nodes.get(0))).thenReturn(nodes.get(1));
        edges.add(edge);
        edge = mock(Edge.class); //1
        when(edge.getTarget(nodes.get(1))).thenReturn(nodes.get(4));
        when(edge.getTarget(nodes.get(4))).thenReturn(nodes.get(1));
        when(edge.getSource(nodes.get(4))).thenReturn(nodes.get(1));
        when(edge.getSource(nodes.get(1))).thenReturn(nodes.get(4));
        edges.add(edge);
        edge = mock(Edge.class); //2
        when(edge.getTarget(nodes.get(4))).thenReturn(nodes.get(6));
        when(edge.getTarget(nodes.get(6))).thenReturn(nodes.get(4));
        when(edge.getSource(nodes.get(6))).thenReturn(nodes.get(4));
        when(edge.getSource(nodes.get(4))).thenReturn(nodes.get(6));
        edges.add(edge);
        edge = mock(Edge.class); //3
        when(edge.getTarget(nodes.get(0))).thenReturn(nodes.get(6));
        when(edge.getTarget(nodes.get(6))).thenReturn(nodes.get(0));
        when(edge.getSource(nodes.get(6))).thenReturn(nodes.get(0));
        when(edge.getSource(nodes.get(0))).thenReturn(nodes.get(6));
        edges.add(edge);
        edge = mock(Edge.class); //4
        when(edge.getTarget(nodes.get(6))).thenReturn(nodes.get(2));
        when(edge.getTarget(nodes.get(2))).thenReturn(nodes.get(6));
        when(edge.getSource(nodes.get(2))).thenReturn(nodes.get(6));
        when(edge.getSource(nodes.get(6))).thenReturn(nodes.get(2));
        edges.add(edge);
        edge = mock(Edge.class); //5
        when(edge.getTarget(nodes.get(2))).thenReturn(nodes.get(1));
        when(edge.getTarget(nodes.get(1))).thenReturn(nodes.get(2));
        when(edge.getSource(nodes.get(1))).thenReturn(nodes.get(2));
        when(edge.getSource(nodes.get(2))).thenReturn(nodes.get(1));
        edges.add(edge);
        edge = mock(Edge.class); //6
        when(edge.getTarget(nodes.get(2))).thenReturn(nodes.get(3));
        when(edge.getTarget(nodes.get(3))).thenReturn(nodes.get(2));
        when(edge.getSource(nodes.get(3))).thenReturn(nodes.get(2));
        when(edge.getSource(nodes.get(2))).thenReturn(nodes.get(3));
        edges.add(edge);
        edge = mock(Edge.class); //7
        when(edge.getTarget(nodes.get(1))).thenReturn(nodes.get(3));
        when(edge.getTarget(nodes.get(3))).thenReturn(nodes.get(1));
        when(edge.getSource(nodes.get(3))).thenReturn(nodes.get(1));
        when(edge.getSource(nodes.get(1))).thenReturn(nodes.get(3));
        edges.add(edge);
        edge = mock(Edge.class); //8
        when(edge.getTarget(nodes.get(0))).thenReturn(nodes.get(3));
        when(edge.getTarget(nodes.get(3))).thenReturn(nodes.get(0));
        when(edge.getSource(nodes.get(3))).thenReturn(nodes.get(0));
        when(edge.getSource(nodes.get(0))).thenReturn(nodes.get(3));
        edges.add(edge);
        edge = mock(Edge.class); //9
        when(edge.getTarget(nodes.get(3))).thenReturn(nodes.get(4));
        when(edge.getTarget(nodes.get(4))).thenReturn(nodes.get(3));
        when(edge.getSource(nodes.get(4))).thenReturn(nodes.get(3));
        when(edge.getSource(nodes.get(3))).thenReturn(nodes.get(4));
        edges.add(edge);

        graph = mock(Graph.class);
        when(graph.getAllNodes()).thenReturn(new HashSet<>(nodes));
        when(graph.getEdges(nodes.get(0))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(0),
                edges.get(3),
                edges.get(8)
        )));
        when(graph.getEdges(nodes.get(1))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(0),
                edges.get(1),
                edges.get(5),
                edges.get(7)
        )));
        when(graph.getEdges(nodes.get(2))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(4),
                edges.get(5),
                edges.get(6)
        )));
        when(graph.getEdges(nodes.get(3))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(6),
                edges.get(7),
                edges.get(8)
        )));
        when(graph.getEdges(nodes.get(4))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(1),
                edges.get(2)
        )));
        when(graph.getEdges(nodes.get(6))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(2),
                edges.get(3),
                edges.get(4)
        )));

        graph2 = mock(Graph.class);
        when(graph2.getAllNodes()).thenReturn(new HashSet<>(nodes));
        when(graph2.getEdges(nodes.get(0))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(0),
                edges.get(3)
        )));
        when(graph2.getEdges(nodes.get(1))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(0),
                edges.get(1),
                edges.get(5),
                edges.get(7)
        )));
        when(graph2.getEdges(nodes.get(2))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(4),
                edges.get(5),
                edges.get(6)
        )));
        when(graph2.getEdges(nodes.get(3))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(6),
                edges.get(7)
        )));
        when(graph2.getEdges(nodes.get(4))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(1),
                edges.get(2)
        )));
        when(graph2.getEdges(nodes.get(6))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(2),
                edges.get(3),
                edges.get(4)
        )));

        graph3 = mock(Graph.class);
        when(graph3.getAllNodes()).thenReturn(new HashSet<>(nodes));
        when(graph3.getAllEdges()).thenReturn(new HashSet<>(edges));
        when(graph3.getEdges(nodes.get(0))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(0),
                edges.get(3)
        )));
        when(graph3.getEdges(nodes.get(1))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(0),
                edges.get(1),
                edges.get(7)
        )));
        when(graph3.getEdges(nodes.get(2))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(4),
                edges.get(6)
        )));
        when(graph3.getEdges(nodes.get(3))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(6),
                edges.get(7)
        )));
        when(graph3.getEdges(nodes.get(4))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(1),
                edges.get(2)
        )));
        when(graph3.getEdges(nodes.get(6))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(2),
                edges.get(3),
                edges.get(4)
        )));

        graph4 = mock(Graph.class);
        when(graph4.getAllNodes()).thenReturn(new HashSet<>(nodes));
        when(graph4.getAllEdges()).thenReturn(new HashSet<>(edges));
        when(graph4.getEdges(nodes.get(0))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(0),
                edges.get(3)
        )));
        when(graph4.getEdges(nodes.get(1))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(0),
                edges.get(1),
                edges.get(7)
        )));
        when(graph4.getEdges(nodes.get(2))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(4),
                edges.get(6)
        )));
        when(graph4.getEdges(nodes.get(3))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(6),
                edges.get(7),
                edges.get(8)
        )));
        when(graph4.getEdges(nodes.get(4))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(1),
                edges.get(2)
        )));
        when(graph4.getEdges(nodes.get(6))).thenReturn(new HashSet<>(Arrays.asList(
                edges.get(2),
                edges.get(3),
                edges.get(4)
        )));
    }

    @Test
    void apply1() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(2)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(2),
                        nodes.get(3)
                ))
        )), new PathStar<Integer>().apply(graph, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(2),
                nodes.get(3)
        )))));
    }

    @Test
    void apply2() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(4),
                        nodes.get(1)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(1),
                        nodes.get(2)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(2),
                        nodes.get(3)
                ))
        )), new PathStar<Integer>().apply(graph, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(4),
                nodes.get(1),
                nodes.get(2),
                nodes.get(3)
        )))));
    }

    @Test
    void apply3() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(2)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(2),
                        nodes.get(3)
                ))
        )), new PathStar<Integer>().apply(graph2, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(2),
                nodes.get(3)
        )))));
    }

    @Test
    void apply4() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(4),
                        nodes.get(1)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(1),
                        nodes.get(2)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(2),
                        nodes.get(3)
                ))
        )), new PathStar<Integer>().apply(graph2, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(4),
                nodes.get(1),
                nodes.get(2),
                nodes.get(3)
        )))));
    }

    @Test
    void apply5() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(2)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(2),
                        nodes.get(3)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(3),
                        nodes.get(0)
                ))
        )), new PathStar<Integer>().apply(graph, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(2),
                nodes.get(3),
                nodes.get(0)
        )))));
    }

    @Test
    void apply6() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(4),
                        nodes.get(1)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(1),
                        nodes.get(2)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(2),
                        nodes.get(3)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(3),
                        nodes.get(0)
                ))
        )), new PathStar<Integer>().apply(graph, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(4),
                nodes.get(1),
                nodes.get(2),
                nodes.get(3),
                nodes.get(0)
        )))));
    }

    @Test
    void apply7() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(2),
                        nodes.get(3),
                        nodes.get(0)
                ))
        )), new PathStar<Integer>().apply(graph3, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(2),
                nodes.get(3),
                nodes.get(0)
        )))));
    }

    @Test
    void apply8() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(2),
                        nodes.get(3)
                ))
        )), new PathStar<Integer>().apply(graph3, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(2),
                nodes.get(3)
        )))));
    }

    @Test
    void apply9() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(2),
                        nodes.get(3)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(3),
                        nodes.get(0)
                ))
        )), new PathStar<Integer>().apply(graph4, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(2),
                nodes.get(3),
                nodes.get(0)
        )))));
    }

    @Test
    void apply10() {
        assertEquals(new HashSet<>(Arrays.asList(
                new Path<>(Arrays.asList(
                        nodes.get(0),
                        nodes.get(6)
                )),
                new Path<>(Arrays.asList(
                        nodes.get(6),
                        nodes.get(2),
                        nodes.get(3)
                ))
        )), new PathStar<Integer>().apply(graph4, new PathStar.Parameters<>(new Path<>(Arrays.asList(
                nodes.get(0),
                nodes.get(6),
                nodes.get(2),
                nodes.get(3)
        )))));
    }
}