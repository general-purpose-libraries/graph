package graph.node;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;

/**
 * Created by chief on 19.03.17.
 */
class DefaultNodeTest {
    @Test
    void getData() {
        Node<Integer> node = new DefaultNode<>(5);
        assertEquals(5, Math.toIntExact(node.getData()));
    }

}