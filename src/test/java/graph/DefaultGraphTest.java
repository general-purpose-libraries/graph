package graph;

import graph.edge.Edge;
import graph.exception.NodeDataNotExsistsException;
import graph.exception.edge.EdgeException;
import graph.node.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by chief on 19.03.17.
 */
class DefaultGraphTest {
    private MutableGraph<Integer> graph;
    private List<Node<Integer>> nodes;
    private List<Edge<Integer>> edges;

    @BeforeEach
    void setUp() throws EdgeException {
        nodes = new LinkedList<>();
        for (int i = 0; i < 10; i++){
            Node<Integer> node = mock(Node.class);
            nodes.add(node);
            when(node.getData()).thenReturn(i);
        }

        EdgeException e = mock(EdgeException.class);

        edges = new LinkedList<>();
        Edge<Integer> edge;
        edge = mock(Edge.class);
        when(edge.getTarget(nodes.get(0))).thenReturn(nodes.get(2));
        when(edge.getSource(nodes.get(2))).thenReturn(nodes.get(0));
        when(edge.getAllNodes()).thenReturn(new HashSet<>(Arrays.asList(
                nodes.get(0),
                nodes.get(2)
        )));
        edges.add(edge);
        edge = mock(Edge.class);
        when(edge.getTarget(nodes.get(2))).thenReturn(nodes.get(1));
        when(edge.getSource(nodes.get(1))).thenReturn(nodes.get(2));
        when(edge.getSource(nodes.get(0))).thenThrow(e);
        when(edge.getAllNodes()).thenReturn(new HashSet<>(Arrays.asList(
                nodes.get(1),
                nodes.get(2)
        )));
        edges.add(edge);
        edge = mock(Edge.class);
        when(edge.getTarget(nodes.get(1))).thenReturn(nodes.get(3));
        when(edge.getSource(nodes.get(3))).thenReturn(nodes.get(1));
        when(edge.getAllNodes()).thenReturn(new HashSet<>(Arrays.asList(
                nodes.get(1),
                nodes.get(3)
        )));
        edges.add(edge);
        edge = mock(Edge.class);
        when(edge.getTarget(nodes.get(3))).thenReturn(nodes.get(6));
        when(edge.getSource(nodes.get(6))).thenReturn(nodes.get(3));
        when(edge.getAllNodes()).thenReturn(new HashSet<>(Arrays.asList(
                nodes.get(3),
                nodes.get(6)
        )));
        edges.add(edge);
        edge = mock(Edge.class);
        when(edge.getTarget(nodes.get(6))).thenReturn(nodes.get(9));
        when(edge.getSource(nodes.get(9))).thenReturn(nodes.get(6));
        when(edge.getAllNodes()).thenReturn(new HashSet<>(Arrays.asList(
                nodes.get(6),
                nodes.get(9)
        )));
        edges.add(edge);

        graph = new DefaultGraph<>(new HashSet<>(nodes), new HashSet<>(edges));
    }

    @Test
    void getEdges1() {
        assertEquals(new HashSet<>(Arrays.asList(
                edges.get(0)
        )), graph.getEdges(nodes.get(0)));
    }

    @Test
    void getEdges2() {
        assertEquals(new HashSet<>(Arrays.asList(
                edges.get(1),
                edges.get(2)
        )), graph.getEdges(nodes.get(1)));
    }

    @Test
    void getEdges3() {
        assertEquals(new HashSet<>(Arrays.asList(
                edges.get(0),
                edges.get(1)
        )), graph.getEdges(nodes.get(2)));
    }

    @Test
    void getEdges4(){
        assertEquals(new HashSet<>(Arrays.asList(
                edges.get(2),
                edges.get(3)
        )), graph.getEdges(nodes.get(3)));
    }

    @Test
    void getEdges5() {
        assertEquals(new HashSet<>(Arrays.asList(
                edges.get(3),
                edges.get(4)
        )), graph.getEdges(nodes.get(6)));
    }

    @Test
    void getEdges6(){
        assertEquals(new HashSet<>(Arrays.asList(
                edges.get(4)
        )), graph.getEdges(nodes.get(9)));
    }

    @Test
    void
    getNodeByData() throws NodeDataNotExsistsException {
        for (int i = 0; i < nodes.size(); i++){
            assertEquals(nodes.get(i), graph.getNodeByData(i));
        }
    }

    @Test
    void addNodes() {
        List<Node<Integer>> nodes = new LinkedList<>();
        for (int i = this.nodes.size(); i < this.nodes.size() + 4; i++){
            Node<Integer> node = mock(Node.class);
            nodes.add(node);
            when(node.getData()).thenReturn(i);
        }

        List<Node<Integer>> allNodes = new ArrayList<>(this.nodes);
        allNodes.addAll(nodes);
        Graph<Integer> newGraph = this.graph.addNodes(new HashSet<>(nodes));
        assertEquals(new HashSet<>(allNodes), newGraph.getAllNodes());
        assertNotEquals(new HashSet<>(this.nodes), newGraph.getAllNodes());
    }

    @Test
    void addEdges() throws EdgeException {
        List<Edge<Integer>> edges = new LinkedList<>();
        Edge<Integer> edge;
        edge = mock(Edge.class);
        when(edge.getTarget(nodes.get(2))).thenReturn(nodes.get(4));
        when(edge.getSource(nodes.get(4))).thenReturn(nodes.get(2));
        when(edge.getAllNodes()).thenReturn(new HashSet<>(Arrays.asList(
                nodes.get(2),
                nodes.get(4)
        )));
        edges.add(edge);
        edge = mock(Edge.class);
        when(edge.getTarget(nodes.get(4))).thenReturn(nodes.get(6));
        when(edge.getSource(nodes.get(6))).thenReturn(nodes.get(4));
        when(edge.getAllNodes()).thenReturn(new HashSet<>(Arrays.asList(
                nodes.get(4),
                nodes.get(6)
        )));
        edges.add(edge);

        Set<Edge<Integer>> allEdges = new HashSet<>(this.edges);
        allEdges.addAll(edges);
        Graph<Integer> newGraph = graph.addEdges(new HashSet<>(edges));
        assertEquals(allEdges, newGraph.getAllEdges());
        assertNotEquals(new HashSet<>(this.edges), newGraph.getAllEdges());
    }

    @Test
    void removeNodes() {
        Set<Node<Integer>> nodesToRemove = new HashSet<>(Arrays.asList(
                nodes.get(1),
                nodes.get(6)
        ));
        Set<Node<Integer>> newSet = new HashSet<>(nodes);
        newSet.removeAll(nodesToRemove);
        Graph<Integer> newGraph = this.graph.removeNodes(nodesToRemove);
        assertEquals(newSet, newGraph.getAllNodes());
        assertNotEquals(new HashSet<>(nodes), newGraph.getAllNodes());
    }

    @Test
    void removeEdges() {
        Set<Edge<Integer>> edgesToRemove = new HashSet<>(Arrays.asList(
                edges.get(2),
                edges.get(3)
        ));
        Set<Edge<Integer>> newSet = new HashSet<>(edges);
        newSet.removeAll(edgesToRemove);
        Graph<Integer> newGraph = this.graph.removeEdges(edgesToRemove);
        assertEquals(newSet, newGraph.getAllEdges());
        assertNotEquals(edges, newGraph.getAllEdges());
    }

    @Test
    void getAllEdges() {
        assertEquals(new HashSet<>(edges), graph.getAllEdges());
    }

    @Test
    void getAllNodes() {
        assertEquals(new HashSet<>(nodes), graph.getAllNodes());
    }

}