package graph.edge;

import graph.exception.edge.DestinationNotExistsException;
import graph.exception.edge.SourceNotExistsException;
import graph.node.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

/**
 * Created by chief on 19.03.17.
 */
class UnidirectionalEdgeTest {
    Node<Integer> source;
    Node<Integer> target;
    UnidirectionalEdge<Integer> edge;

    @BeforeEach
    void setUp() {
        source = mock(Node.class);
        target = mock(Node.class);
        edge = new UnidirectionalEdge<>(source, target);
    }

    @Test
    void getSource() throws SourceNotExistsException {
        assertEquals(source, edge.getSource(target));
    }

    @Test
    void getTarget() throws DestinationNotExistsException {
        assertEquals(target, edge.getTarget(source));
    }

    @Test
    void getAllNodes() {
        assertEquals(new HashSet<>(Arrays.asList(
                source,
                target
        )), edge.getAllNodes());
    }

}