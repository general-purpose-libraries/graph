package graph.edge;

import graph.node.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

/**
 * Created by chief on 19.03.17.
 */
class BidirectionalWeightedEdgeTest {
    Node<Integer> source;
    Node<Integer> target;
    BidirectionalWeightedEdge<Integer> edge;

    @BeforeEach
    void setUp() {
        source = mock(Node.class);
        target = mock(Node.class);
        edge = new BidirectionalWeightedEdge<>(source, target, BigDecimal.TEN);
    }
    @Test
    void getWeight() {
        assertTrue(edge.getWeight().compareTo(BigDecimal.TEN) == 0);
    }

}