package graph.edge;

import graph.node.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

/**
 * Created by chief on 19.03.17.
 */
class CircularWeightedEdgeTest {
    Node<Integer> source;
    CircularWeightedEdge<Integer> edge;

    @BeforeEach
    void setUp() {
        source = mock(Node.class);
        edge = new CircularWeightedEdge<>(source, BigDecimal.TEN);
    }

    @Test
    void getWeight() {
        assertTrue(edge.getWeight().compareTo(BigDecimal.TEN) == 0);
    }

}