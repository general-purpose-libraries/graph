package graph.edge;

import graph.exception.edge.EdgeException;
import graph.node.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

/**
 * Created by chief on 19.03.17.
 */
class CircularEdgeTest {
    Node<Integer> source;
    CircularEdge<Integer> edge;

    @BeforeEach
    void setUp() {
        source = mock(Node.class);
        edge = new CircularEdge<>(source);
    }

    @Test
    void getSource() throws EdgeException {
        assertEquals(source, edge.getSource(source));
    }

    @Test
    void getTarget() throws EdgeException {
        assertEquals(source, edge.getTarget(source));
    }

    @Test
    void getAllNodes() {
        assertEquals(Collections.singleton(source), edge.getAllNodes());
    }

}