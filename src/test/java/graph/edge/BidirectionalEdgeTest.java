package graph.edge;

import graph.exception.edge.EdgeException;
import graph.node.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

/**
 * Created by chief on 19.03.17.
 */
class BidirectionalEdgeTest{
    Node<Integer> source;
    Node<Integer> target;
    BidirectionalEdge<Integer> edge;

    @BeforeEach
    void setUp() {
        source = mock(Node.class);
        target = mock(Node.class);
        edge = new BidirectionalEdge<>(source, target);
    }

    @Test
    void getSource() throws EdgeException {
        assertEquals(source, edge.getSource(target));
        assertEquals(target, edge.getSource(source));
    }

    @Test
    void getTarget() throws EdgeException {
        assertEquals(source, edge.getSource(target));
        assertEquals(target, edge.getSource(source));
    }

    @Test
    void getAllNodes() {
        assertEquals(new HashSet<>(Arrays.asList(
                source,
                target
        )), edge.getAllNodes());
    }
}