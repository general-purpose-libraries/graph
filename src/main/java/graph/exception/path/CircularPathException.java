package graph.exception.path;

import graph.helperclasses.Path;
import graph.node.Node;

/**
 * Created by chief on 19.03.17.
 */
public class CircularPathException extends Exception {
    private final Path<Node> path;
    private final Node node;

    public CircularPathException(Path path, Node node) {
        assert path != null;
        assert node != null;
        this.path = path;
        this.node = node;
    }

    @Override
    public String getMessage() {
        return String.format("Node %s already exists in path %s", node, path);
    }
}
