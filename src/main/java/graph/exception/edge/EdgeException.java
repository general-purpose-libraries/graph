package graph.exception.edge;

import graph.node.Node;

/**
 * This exception represents errors occuring related to edges
 */
public abstract class EdgeException extends Exception {
    protected final Node node;

    protected EdgeException(Node node) {
        assert node != null;
        this.node = node;
    }
}
