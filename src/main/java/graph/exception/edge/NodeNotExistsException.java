package graph.exception.edge;

import graph.exception.edge.EdgeException;
import graph.node.Node;

/**
 * This exception is thrown if a node not exists in an edge
 */
public class NodeNotExistsException extends EdgeException {
    public NodeNotExistsException(Node node) {
        super(node);
    }

    @Override
    public String getMessage() {
        return String.format("Node does not exist: %s", node);
    }
}
