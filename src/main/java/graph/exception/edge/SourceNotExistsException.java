package graph.exception.edge;

import graph.exception.edge.EdgeException;
import graph.node.Node;

/**
 * This exception is thrown when a source not exists for a node of an edge
 */
public class SourceNotExistsException extends EdgeException {
    public SourceNotExistsException(Node node) {
        super(node);
    }

    @Override
    public String getMessage() {
        return String.format("Source node does not exist for node %s", node);
    }
}
