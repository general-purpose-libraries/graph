package graph.exception.edge;

import graph.exception.edge.EdgeException;
import graph.node.Node;

/**
 * This exception is thrown when a destination not exists for a node of an edge
 */
public class DestinationNotExistsException extends EdgeException {
    public DestinationNotExistsException(Node node) {
        super(node);
    }

    @Override
    public String getMessage() {
        return String.format("Destination node does not exist for node %s", node);
    }
}
