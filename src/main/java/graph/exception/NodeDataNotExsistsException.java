package graph.exception;

/**
 * This exception is thrown if no node contains a given data
 */
public class NodeDataNotExsistsException extends Exception{
    private final Object data;

    public NodeDataNotExsistsException(Object data) {
        assert data != null;
        this.data = data;
    }

    @Override
    public String getMessage() {
        return String.format("No node with data exists: %s", data);
    }
}
