package graph.node;

/**
 * This class represents a node stored in the main memory
 */
public class DefaultNode<V> implements Node<V> {
    private final V data;

    public DefaultNode(V data) {
        assert data != null;
        this.data = data;
    }

    /**
     * @return Data stored in this node
     */
    @Override
    public V getData(){
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DefaultNode<?> that = (DefaultNode<?>) o;

        return data != null ? data.equals(that.data) : that.data == null;
    }

    @Override
    public int hashCode() {
        return data != null ? data.hashCode() : 0;
    }
}
