package graph.node;

import graph.edge.Edge;

import java.util.HashSet;
import java.util.Set;

/**
 * This class represents a node stored in a graph
 * @param <V> Data type stored in the node
 */
public interface Node<V> {
    /**
     * @return Data stored in this node
     */
    V getData();
}
