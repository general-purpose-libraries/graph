package graph;

import graph.edge.Edge;
import graph.node.Node;

import java.util.Set;

/**
 * This abstract class simplifies implementation
 */
public abstract class AbstractGraph<V> implements Graph<V> {
    /**
     * Creates a new graph with nodes of type V
     * @param nodes Set containing all nodes of the graph
     * @return The new graph
     */
    protected AbstractGraph(Set<Node<V>> nodes, Set<Edge<V>> edges){
        assert nodes != null;
        assert edges != null;
        assert edgesValid(edges, nodes);
    }

    private boolean edgesValid(Set<Edge<V>> edges, Set<Node<V>> nodes){
        for (Edge<V> edge : edges){
            for (Node<V> node : edge.getAllNodes()){
                if (!nodes.contains(node)){
                    return false;
                }
            }
        }
        return true;
    }
}
