package graph.helperclasses;

import graph.Graph;
import graph.edge.Edge;
import graph.node.Node;
import graph.visualization.Layouter;
import graph.visualization.Visualizer;

/**
 * Static methods for sanity checks
 */
public class SanityCheck {
    public static <V, P> void checkParameters(Graph<V> graph, int parametersRequired, P... parameters){
        if (graph == null){
            throw new NullPointerException("Parameter 'graph' is null.");
        }
        if (parameters == null){
            throw new NullPointerException("Parameter 'parameters' is null.");
        }
        if (parameters.length < parametersRequired){
            throw new ArrayIndexOutOfBoundsException("Parameter 'parameters' must contain at least one element.");
        }
    }

    public static <V> void checkParameters(Node<V> node, int width, int height){
        if (node == null){
            throw new NullPointerException("Parameter 'node' is null.");
        }
        if (width < 0){
            throw new IllegalArgumentException("Parameter 'width' must not be smaller than zero.");
        }
        if (height < 0){
            throw new IllegalArgumentException("Parameter 'height' must not be smaller than zero.");
        }
    }

    public static <V> void checkParameters(Edge<V> edge, Visualizer.NodeDimension<V> nodeDimension1, Visualizer.NodeDimension<V> nodeDimension2){
        if (edge == null){
            throw new NullPointerException("Parameter 'edge' is null.");
        }
        if (nodeDimension1 == null){
            throw new NullPointerException("Parameter 'nodeDimension1' is null.");
        }
        if (nodeDimension2 == null){
            throw new NullPointerException("Parameter 'nodeDimension2' is null.");
        }
        if (!edge.getAllNodes().contains(nodeDimension1.getNode())){
            throw new IllegalArgumentException("Node of Parameter 'nodeDimension1' is not part of the edge.");
        }
        if (!edge.getAllNodes().contains(nodeDimension2.getNode())){
            throw new IllegalArgumentException("Node of Parameter 'nodeDimension2' is not part of the edge.");
        }
    }

    public static <V> void checkParameters(Node<V> node, Layouter.Position position){
        if (node == null){
            throw new NullPointerException("Parameter 'node' is null.");
        }
        if (position == null){
            throw new NullPointerException("Parameter 'position' is null.");
        }
    }

    public static <V> void checkParameters(Edge<V> edge, Layouter.NodePosition<V> nodePosition1, Layouter.NodePosition<V> nodePosition2){
        if (edge == null){
            throw new NullPointerException("Parameter 'edge' is null.");
        }
        if (nodePosition1 == null){
            throw new NullPointerException("Parameter 'nodePosition1' is null.");
        }
        if (nodePosition2 == null){
            throw new NullPointerException("Parameter 'nodePosition2' is null.");
        }
        if (!edge.getAllNodes().contains(nodePosition1.getNode())){
            throw new IllegalArgumentException("Node of Parameter 'nodePosition1' is not part of the edge.");
        }
        if (!edge.getAllNodes().contains(nodePosition2.getNode())){
            throw new IllegalArgumentException("Node of Parameter 'nodePosition2' is not part of the edge.");
        }
    }
}
