package graph.helperclasses;

import graph.exception.path.CircularPathException;
import graph.node.Node;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by chief on 19.03.17.
 */
public class Path<V> implements Iterable<Node<V>>, Comparable<Path<V>>{
    private final List<Node<V>> nodes;
    private final Map<Node<V>, Node<V>> predecessors;
    private Node<V> end;
    private final int size;

    public Path(List<Node<V>> nodes) {
        assert nodes != null;
        assert nodes.size() > 0;
        this.nodes = new LinkedList<>();
        this.predecessors = new ConcurrentHashMap<>();

        Iterator<Node<V>> nodeIterator = nodes.iterator();
        Node<V> predecessor = null;
        while (nodeIterator.hasNext()){
            Node<V> node = nodeIterator.next();

            if (predecessor != null){
                assert !node.equals(predecessor);
                predecessors.put(node, predecessor);
            }

            this.nodes.add(node);
            if (!nodeIterator.hasNext()){
                end = node;
            }

            predecessor = node;
        }
        this.size = this.nodes.size();
    }

    public List<Node<V>> getNodes() {
        return nodes;
    }

    public Node<V> getEndNode(){
        return end;
    }

    public Path<V> attachNode(Node<V> node) throws CircularPathException {
        if (this.nodes.contains(node)){
            throw new CircularPathException(this, node);
        }
        List<Node<V>> nodes = new LinkedList<>(this.nodes);
        nodes.add(node);
        return new Path<>(nodes);
    }

    public Node<V> getPredecessor(Node<V> node){
        assert node != null;
        assert nodes.contains(node);
        if (predecessors.containsKey(node)){
            return predecessors.get(node);
        }
        return null;
    }

    public int size(){
        return size;
    }

    @Override
    public int compareTo(Path<V> path) {
        if (Objects.equals(this, path)){
            return 0;
        }
        if (this.size() < path.size()){
            return -1;
        }
        return 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Path<?> path = (Path<?>) o;

        if (nodes != null ? !nodes.equals(path.nodes) : path.nodes != null) return false;
        return end != null ? end.equals(path.end) : path.end == null;
    }

    @Override
    public int hashCode() {
        int result = nodes != null ? nodes.hashCode() : 0;
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Path{" +
                "nodes=" + nodes +
                '}';
    }

    @Override
    public Iterator<Node<V>> iterator() {
        List<Node<V>> nodes = new LinkedList<>(this.nodes);
        return nodes.iterator();
    }
}
