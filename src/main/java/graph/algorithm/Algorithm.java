package graph.algorithm;

import graph.Graph;

/**
 * This interface describes an graph.algorithm that can be applied to a graph
 * @param <R> Result type of the graph.algorithm
 * @param <V> Type of data stored in the nodes of the graph
 * @param <P> Type of parameters the graph.algorithm takes
 */
public interface Algorithm<R, V, P> {
    /**
     * Applies the graph.algorithm to the given graph
     * @param graph graph.Graph the graph.algorithm shall be applied to
     * @param parameters Parameters for the graph.algorithm
     * @return Result of the graph.algorithm
     */
    R apply(Graph<V> graph, P... parameters);
}
