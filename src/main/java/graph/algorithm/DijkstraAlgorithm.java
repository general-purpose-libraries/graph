package graph.algorithm;

import graph.Graph;
import graph.edge.Edge;
import graph.edge.WeightedEdge;
import graph.exception.edge.DestinationNotExistsException;
import graph.exception.edge.EdgeException;
import graph.helperclasses.Path;
import graph.helperclasses.SanityCheck;
import graph.node.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This algorithm finds the shortest path between to nodes in a graph
 */
public class DijkstraAlgorithm<V> implements Algorithm<Path<V>, V, DijkstraAlgorithm.Parameters<V>>{
    private static Logger LOGGER = LoggerFactory.getLogger(DijkstraAlgorithm.class);

    public static class Parameters<V> {
        private final Node<V> start;
        private final Node<V> end;

        public Parameters(Node<V> start, Node<V> end) {
            assert start != null;
            assert end != null;
            this.start = start;
            this.end = end;
        }

        public Node<V> getStart() {
            return start;
        }

        public Node<V> getEnd() {
            return end;
        }
    }

    private static class InfiniteBigDecimal extends BigDecimal {

        private static final BigDecimal value = BigDecimal.ZERO;

        public InfiniteBigDecimal(){
            super(value.toBigInteger());
        }

        @Override
        public String toString() {
            return "∞";
        }

        @Override
        public boolean equals(Object o) {
            return o.getClass() == InfiniteBigDecimal.class;
        }

        @Override
        public int hashCode() {
            return value.hashCode();
        }

        @Override
        public int compareTo(BigDecimal bigDecimal) {
            if (bigDecimal.getClass() == InfiniteBigDecimal.class){
                return 0;
            }
            return 1;
        }
    }

    @SafeVarargs
    @Override
    public final Path<V> apply(Graph<V> graph, DijkstraAlgorithm.Parameters<V>... parameters) {
        LOGGER.trace("Applying algorithm...");
        SanityCheck.checkParameters(graph, 1, parameters);
        if (!graph.getAllNodes().contains(parameters[0].getStart())){
            throw new IllegalArgumentException("Start node is not part of the provided graph.");
        }
        if (!graph.getAllNodes().contains(parameters[0].getEnd())){
            throw new IllegalArgumentException("End node is not part of the provided graph.");
        }

        Parameters<V> params = parameters[0];
        Set<Node<V>> nodes = graph.getAllNodes();
        Map<Node<V>, BigDecimal> nodeDistances = new ConcurrentHashMap<>(nodes.size());

        Map<Node<V>, Node<V>> parent = traceGraph(
                graph, params, nodeDistances, initializeQueue(graph, params, nodeDistances)
        );
        List<Node<V>> path = collectPath(params, parent);

        return new Path<>(path);
    }

    private List<Node<V>> collectPath(Parameters<V> params, Map<Node<V>, Node<V>> parent) {
        List<Node<V>> path = new LinkedList<>();
        Node<V> node = params.getEnd();

        if (parent.containsKey(node)){
            path.add(node);
            while (parent.containsKey(node) && !node.equals(params.getStart())){
                Node<V> next = parent.get(node);
                path.add(next);
                node = next;
            }
            Collections.reverse(path);
        }
        return path;
    }

    private Map<Node<V>, Node<V>> traceGraph(Graph<V> graph, Parameters<V> params, Map<Node<V>, BigDecimal> nodeDistances, List<Node<V>> queue) {
        LOGGER.debug("Beginning tracing graph...");
        Map<Node<V>, Node<V>> parent = new HashMap<>();
        while (!queue.isEmpty()){
            Node<V> node = queue.remove(0);
            LOGGER.trace("Removed first node of the queue.");

            if (node.equals(params.getEnd())){
                LOGGER.trace("Found end node. Finished tracing.");
                break;
            }

            BigDecimal distance = nodeDistances.get(node);
            Set<Edge<V>> nextNodes = graph.getEdges(node);
            nextNodes.parallelStream().forEach(edge -> {
                BigDecimal newEdgeDistance = calculateNewDistance(distance, edge);

                Node<V> connectedNode;
                try {
                    connectedNode = edge.getTarget(node);
                }
                catch (DestinationNotExistsException e){
                    LOGGER.trace("Current edge has no target node. Skipping current edge.");
                    return;
                }
                catch (EdgeException e) {
                    throw new RuntimeException(e);
                }

                BigDecimal edgeDistance = nodeDistances.get(connectedNode);
                if (edgeDistance.equals(new InfiniteBigDecimal())){
                    LOGGER.trace("Target node is visited for first time.");
                    nodeDistances.put(connectedNode, newEdgeDistance);
                    sortQueue(nodeDistances, queue);
                    parent.put(connectedNode, node);
                    LOGGER.trace("Set target node as parent of the current node.");
                }
                else {
                    if (newEdgeDistance.compareTo(edgeDistance) == -1){
                        LOGGER.trace("Target node is nearer than the current parent node. Updating...");
                        nodeDistances.put(connectedNode, newEdgeDistance);
                        sortQueue(nodeDistances, queue);
                        parent.put(connectedNode, node);
                        LOGGER.trace("Set target node as parent of the current node.");
                    }
                }
            });
        }
        return parent;
    }

    private BigDecimal calculateNewDistance(BigDecimal distance, Edge<V> edge) {
        LOGGER.trace("Calculating new edge distance...");
        BigDecimal newEdgeDistance = distance;
        if (edge instanceof WeightedEdge){
            newEdgeDistance = newEdgeDistance.add(((WeightedEdge)edge).getWeight());
        }
        else {
            newEdgeDistance = newEdgeDistance.add(BigDecimal.ONE);
        }
        LOGGER.trace(String.format("New edge distance: %s", newEdgeDistance));
        return newEdgeDistance;
    }

    private List<Node<V>> initializeQueue(Graph<V> graph, Parameters<V> params, Map<Node<V>, BigDecimal> nodeDistances) {
        LOGGER.debug("Initializing queue...");
        List<Node<V>> queue = Collections.synchronizedList(new LinkedList<>());
        for (Node<V> node : graph.getAllNodes()){
            if (node.equals(params.getStart())){
                nodeDistances.put(node, BigDecimal.ZERO);
                LOGGER.trace("Set distance to starting node to zero.");
            }
            else {
                nodeDistances.put(node, new InfiniteBigDecimal());
                LOGGER.trace("Set distance to graph node to infinite.");
            }
            queue.add(node);
            LOGGER.trace("Node has been added to queue.");
        }
        sortQueue(nodeDistances, queue);
        return queue;
    }

    private void sortQueue(Map<Node<V>, BigDecimal> nodeDistances, List<Node<V>> queue) {
        LOGGER.trace("Sorting queue ascending by distance...");
        queue.sort((node1, node2) -> {
            BigDecimal n1 = nodeDistances.get(node1);
            BigDecimal n2 = nodeDistances.get(node2);
            if (n1.getClass() == InfiniteBigDecimal.class) {
                if (n2.getClass() == InfiniteBigDecimal.class) {
                    return 0;
                }
                return 1;
            }
            if (n2.getClass() == InfiniteBigDecimal.class) {
                return -1;
            }

            return n1.compareTo(n2);
        });
        LOGGER.trace("Queue has been sorted.");
    }
}
