package graph.algorithm;

import graph.Graph;
import graph.edge.Edge;
import graph.exception.edge.DestinationNotExistsException;
import graph.exception.edge.EdgeException;
import graph.helperclasses.Path;
import graph.helperclasses.SanityCheck;
import graph.node.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * This algorithm splits a given path into subpaths wich start
 * and end at nodes which belong to multiple edges.
 */
public class PathStar<V> implements Algorithm<Set<Path<V>>, V, PathStar.Parameters<V>> {
    private static Logger LOGGER = LoggerFactory.getLogger(DijkstraAlgorithm.class);

    public static class Parameters<V> {
        private final Path<V> pathToSplit;

        public Parameters(Path<V> pathToSplit) {
            assert pathToSplit != null;
            this.pathToSplit = pathToSplit;
        }

        public Path<V> getPathToSplit() {
            return pathToSplit;
        }
    }

    @SafeVarargs
    @Override
    public final Set<Path<V>> apply(Graph<V> graph, PathStar.Parameters<V>... parameters) {
        LOGGER.trace("Applying algorithm...");
        SanityCheck.checkParameters(graph, 1, parameters);
        assert graph.getAllNodes().containsAll(parameters[0].getPathToSplit().getNodes());

        PathStar.Parameters<V> params = parameters[0];
        Set<Path<V>> paths = new HashSet<>();

        List<Node<V>> subPath = new LinkedList<>();
        Iterator<Node<V>> pathIterator = params.getPathToSplit().iterator();
        while (pathIterator.hasNext()){
            Node<V> node = pathIterator.next();
            subPath.add(node);

            Set<Edge<V>> edges = new HashSet<>(graph.getEdges(node));
            if (pathIterator.hasNext()){
                edges.removeAll(findVisitedEdges(graph, params, node));
            }

            LOGGER.trace("Checking if path can be splitted at current node...");
            if (edges.size() > 1){
                if (subPath.size() > 1){
                    paths.add(new Path<>(subPath));
                    LOGGER.trace("Split path at current node.");
                }
                if (!pathIterator.hasNext()){
                    subPath = new LinkedList<>();
                    break;
                }
                subPath = new LinkedList<>(Collections.singleton(node));
            }
        }
        if (!subPath.isEmpty()){
            subPath.add(params.getPathToSplit().iterator().next());
            paths.add(new Path<>(subPath));
        }

        return paths;
    }

    private Set<Edge<V>> findVisitedEdges(Graph<V> graph, Parameters<V> params, Node<V> node) {
        LOGGER.trace("Searching for edges already visited...");
        Set<Edge<V>> edgesVisited = new ConcurrentSkipListSet<>((edge, edge2) -> {
            if (Objects.equals(edge, edge2)){
                return 0;
            }
            return 1;
        });
        graph.getEdges(node).parallelStream().forEach(edge -> {
            Node<V> previousNode = params.getPathToSplit().getPredecessor(node);

            try {
                if (previousNode != null && edge.getTarget(node).equals(previousNode)){
                    edgesVisited.add(edge);
                    LOGGER.trace("Found edges that already have been visited.");
                }
            }
            catch (DestinationNotExistsException ignored){}
            catch (EdgeException e) {
                throw new RuntimeException(e);
            }
        });
        /*for (Edge<V> edge : graph.getEdges(node)){
            Node<V> previousNode = params.getPathToSplit().getPredecessor(node);

            try {
                if (previousNode != null && edge.getTarget(node).equals(previousNode)){
                    edgesVisited.add(edge);
                    LOGGER.trace("Found edges that already have been visited.");
                }
            }
            catch (DestinationNotExistsException ignored){}
            catch (EdgeException e) {
                throw new RuntimeException(e);
            }
        }*/
        LOGGER.trace("Search done.");
        return edgesVisited;
    }
}
