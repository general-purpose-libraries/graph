package graph.algorithm;

import graph.Graph;
import graph.exception.edge.EdgeException;
import graph.exception.path.CircularPathException;
import graph.helperclasses.Path;
import graph.helperclasses.SanityCheck;
import graph.node.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * This algorithm finds all paths between two given nodes
 */
public class GraphWalker<V> implements Algorithm<Set<Path<V>>, V, GraphWalker.Parameters<V>> {
    private static Logger LOGGER = LoggerFactory.getLogger(GraphWalker.class);

    public static class Parameters<V> {
        private final Node<V> start;
        private final Node<V> end;

        public Parameters(Node<V> start, Node<V> end) {
            assert start != null;
            assert end != null;
            this.start = start;
            this.end = end;
        }

        public Node<V> getStart() {
            return start;
        }

        public Node<V> getEnd() {
            return end;
        }
    }

    private static class TracingResult<V> {
        private final Set<Path<V>> paths;
        private final boolean isFinished;

        public static <V> TracingResult<V> pathFinished(Path<V> path){
            return new TracingResult<>(Collections.singleton(path), true);
        }

        public static <V> TracingResult<V> pathsNotFinished(Set<Path<V>> paths){
            return new TracingResult<>(paths, false);
        }

        private TracingResult(Set<Path<V>> paths, boolean isFinished) {
            assert paths != null;
            this.paths = paths;
            this.isFinished = isFinished;
        }

        public Set<Path<V>> getPaths() {
            return paths;
        }

        public boolean isFinished() {
            return isFinished;
        }
    }

    @SafeVarargs
    @Override
    public final Set<Path<V>> apply(Graph<V> graph, GraphWalker.Parameters<V>... parameters) {
        LOGGER.debug("Applying algorithm...");
        SanityCheck.checkParameters(graph, 1, parameters);
        if (!graph.getAllNodes().contains(parameters[0].getStart())){
            throw new IllegalArgumentException("Start node is not part of the provided graph.");
        }
        if (!graph.getAllNodes().contains(parameters[0].getEnd())){
            throw new IllegalArgumentException("End node is not part of the provided graph.");
        }

        Parameters<V> params = parameters[0];
        Set<Path<V>> currentPaths = new ConcurrentSkipListSet<>();
        currentPaths.add(new Path<>(Collections.singletonList(params.getStart())));
        Set<Path<V>> pathsFound = new ConcurrentSkipListSet<>();

        while(!currentPaths.isEmpty()){
            Set<Path<V>> nextPaths = new ConcurrentSkipListSet<>();
            currentPaths.parallelStream().forEach(path -> {
                LOGGER.trace("Tracing path...");
                TracingResult<V> tracingResult = tracePath(path, params, graph);
                if (tracingResult.isFinished()){
                    LOGGER.trace("Path finished.");
                    assert tracingResult.getPaths().size() == 1;
                    pathsFound.add(tracingResult.getPaths().iterator().next());
                    return;
                }
                nextPaths.addAll(tracingResult.getPaths());
            });
            currentPaths = new ConcurrentSkipListSet<>(nextPaths);
            nextPaths.clear();
        }

        return pathsFound;
    }

    private TracingResult<V> tracePath(Path<V> path, Parameters<V> parameters, Graph<V> graph){
        Node<V> node = path.getEndNode();

        if (node.equals(parameters.getEnd())){
            LOGGER.trace("Found paths to the end node. Added to the list of found paths.");
            return TracingResult.pathFinished(path);
        }

        Set<Path<V>> nextPaths = new ConcurrentSkipListSet<>();
        graph.getEdges(node).parallelStream().forEach(edge -> {
            Node<V> nextNode;
            try {
                nextNode = edge.getTarget(node);
                nextPaths.add(path.attachNode(nextNode));
            } catch (EdgeException | CircularPathException ignored){}
        });
        return TracingResult.pathsNotFinished(nextPaths);
    }
}
