package graph.visualization;

import graph.edge.Edge;
import graph.exception.edge.NodeNotExistsException;
import graph.helperclasses.SanityCheck;
import graph.node.Node;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This interface layouts the nodes and edges of a graph to draw
 * @param <V> Type of the nodes of the graph to draw
 */
public interface Layouter<V> {

    /**
     * Position of the object to draw
     */
    class Position {
        private final BigInteger x;
        private final BigInteger y;

        public Position(BigInteger x, BigInteger y) {
            assert x != null;
            assert y != null;
            this.x = x;
            this.y = y;
        }

        public BigInteger getX(){
            return x;
        }

        public BigInteger getY(){
            return y;
        }
    }

    /**
     * Position at which the node shall be placed on a rendered image
     * @param <V> Type of the node
     */
    class NodePosition<V> {
        private final Node<V> node;
        private final Position position;

        public NodePosition(Node<V> node, Position position) {
            SanityCheck.checkParameters(node, position);
            this.node = node;
            this.position = position;
        }

        public Node<V> getNode() {
            return node;
        }

        public Position getPosition() {
            return position;
        }
    }

    /**
     * Position at which the edge shall be placed on a rendered image
     * @param <V> Type of the nodes of the edge
     */
    class EdgePosition<V>{
        private final Edge<V> edge;
        private final Map<Node<V>, NodePosition<V>> nodePositions;

        public EdgePosition(Edge<V> edge, NodePosition<V> nodePosition1, NodePosition<V> nodePosition2) {
            SanityCheck.checkParameters(edge, nodePosition1, nodePosition2);
            this.edge = edge;
            this.nodePositions = new ConcurrentHashMap<>();
            this.nodePositions.put(nodePosition1.getNode(), nodePosition1);
            this.nodePositions.put(nodePosition2.getNode(), nodePosition2);
        }

        Edge<V> getEdge(){
            return edge;
        }

        /**
         * Returns the position at which the edge is connected to the given node
         * @param node Node to retrieve the connection for
         * @return The node position of the connection
         */
        NodePosition getStartPosition(Node<V> node) throws NodeNotExistsException{
            if (!nodePositions.containsKey(node)){
                throw new NodeNotExistsException(node);
            }
            return nodePositions.get(node);
        }
    }

    /**
     * Returns the position where the given Node shall be drawn onto the image
     * @param nodeDimension Dimensions the node shall have on the drawn image
     * @return The position of the node where it should be drawn onto the image
     *         (top left corner)
     */
    NodePosition<V> getPosition(Visualizer.NodeDimension<V> nodeDimension);

    EdgePosition<V> getPosition(Visualizer.EdgeDimension<V> edgeDimension);
}
