package graph.visualization;

import graph.Graph;

import java.awt.*;

/**
 * This interface renders graphs onto images.
 */
public interface Renderer {
    <V> Image render(Graph<V> graph, Layouter layouter, Visualizer visualizer);
}
