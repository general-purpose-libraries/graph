package graph.visualization;

import graph.edge.Edge;
import graph.exception.edge.NodeNotExistsException;
import graph.helperclasses.SanityCheck;
import graph.node.Node;

import java.awt.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This interface renders edges and nodes of a graph.
 * The rendered tiles must then be placed by the Renderer
 * onto the resulting image by using the Layouter to determine
 * the positions on the image
 */
public interface Visualizer {

    class NodeDimension<V> {
        private final Node<V> node;
        private final int width;
        private final int height;

        public NodeDimension(Node<V> node, int width, int height) {
            SanityCheck.checkParameters(node, width, height);
            this.node = node;
            this.width = width;
            this.height = height;
        }

        public Node<V> getNode(){
            return node;
        }

        public int getWidth(){
            return width;
        }

        public int getHeight(){
            return height;
        }
    }

    class EdgeDimension<V> {
        private final Edge<V> edge;
        private final Map<Node<V>, NodeDimension<V>> nodeDimension;

        public EdgeDimension(Edge<V> edge, NodeDimension<V> nodeDimension1, NodeDimension<V> nodeDimension2) {
            SanityCheck.checkParameters(edge, nodeDimension1, nodeDimension2);
            this.edge = edge;
            this.nodeDimension = new ConcurrentHashMap<>();
            this.nodeDimension.put(nodeDimension1.getNode(), nodeDimension1);
            this.nodeDimension.put(nodeDimension2.getNode(), nodeDimension2);
        }

        public Edge<V> getEdge(){
            return edge;
        }

        /**
         * @param node Node which dimension shall be retrieved
         * @return Dimension of the given node
         * @throws NodeNotExistsException If the node is not part of the edge
         */
        public NodeDimension<V> getNodeDimension(Node<V> node) throws NodeNotExistsException{
            assert node != null;
            if (!nodeDimension.containsKey(node)){
                throw new NodeNotExistsException(node);
            }
            return nodeDimension.get(node);
        }
    }

    /**
     * Renders a node into a tile
     * @param node Node to render
     * @param <V> Type of the node
     * @return The rendered tile
     */
    <V> Image render(Node<V> node);

    /**
     * Renders an edge into a tile
     * @param position Positions of the connections to the nodes of the edge
     * @param <V> Type of the nodes of the edge
     * @return The rendered tile
     */
    <V> Image render(Layouter.EdgePosition<V> position);
}
