package graph;

import graph.edge.Edge;
import graph.node.Node;

import java.util.Set;

/**
 * This class describes a graph
 * @param <V> Type of data that shall be stored in the nodes of the graph
 */
public interface MutableGraph<V> extends Graph<V> {
    /**
     * Adds the given nodes to the graph
     * @param nodes Nodes to be added to the graph
     * @return A new graph containing with the given nodes added
     */
    Graph<V> addNodes(Set<Node<V>> nodes);

    /**
     * Adds the given edges to the graph
     * @param edges Edges to be added to the graph
     * @return A new graph containing with the given edges added
     */
    Graph<V> addEdges(Set<Edge<V>> edges);

    /**
     * Removes the given nodes from the graph
     * @param nodes Nodes to be removed from the graph
     * @return A new graph with the given nodes removed
     */
    Graph<V> removeNodes(Set<Node<V>> nodes);

    /**
     * Removes the given edges from the graph
     * @param edges Edges to be removed from the graph
     * @return A new graph with the given edges removed
     */
    Graph<V> removeEdges(Set<Edge<V>> edges);
}
