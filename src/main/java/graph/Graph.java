package graph;

import graph.algorithm.Algorithm;
import graph.edge.Edge;
import graph.exception.NodeDataNotExsistsException;
import graph.node.Node;
import graph.visualization.Layouter;
import graph.visualization.Renderer;
import graph.visualization.Visualizer;

import java.awt.*;
import java.util.Set;

/**
 * This class describes a graph
 * @param <V> Type of data that shall be stored in the nodes of the graph
 */
public interface Graph<V> {

    /**
     * Get all edges containing the given node
     * @param node Node to get edges for
     * @return Set of edges having been found
     */
    Set<Edge<V>> getEdges(Node<V> node);

    /**
     * Get the node having the given data
     * @param data The data returned node shall have
     * @return Node having the given data
     */
    Node<V> getNodeByData(V data) throws NodeDataNotExsistsException;

    /**
     * Applies an graph.algorithm to the graph
     * @param algorithm The graph.algorithm to be applied to the graph
     * @param parameter Parameters for the graph.algorithm
     * @param <R> The result type of the graph.algorithm
     * @param <P> The Parameter type of the graph.algorithm
     * @return
     */
    default <R,P> R apply(Algorithm<R, V, P> algorithm, P... parameter){
        return algorithm.apply(this, parameter);
    }

    /**
     * Renders the current graph into an image
     * @param renderer The renderer to be used for rendering the graph onto an image
     * @param layouter A layouter for setting the positions of the nodes on the image
     * @param visualizer A visualizer for coloring the nodes and edges
     * @return Rendered image of the graph
     */
    default Image render(Renderer renderer, Layouter layouter, Visualizer visualizer){
        return renderer.render(this,layouter, visualizer);
    }

    /**
     * @return All Edges the graph has
     */
    Set<Edge<V>> getAllEdges();

    /**
     * @return All Nodes the graph has
     */
    Set<Node<V>> getAllNodes();
}
