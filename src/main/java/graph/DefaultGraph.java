package graph;

import graph.edge.Edge;
import graph.exception.NodeDataNotExsistsException;
import graph.node.Node;

import java.util.HashSet;
import java.util.Set;

/**
 * This is the default implementation of a mutable graph
 */
public class DefaultGraph<V> extends AbstractGraph<V> implements MutableGraph<V> {
    private final Set<Node<V>> nodes;
    private final Set<Edge<V>> edges;

    public DefaultGraph(Set<Node<V>> nodes, Set<Edge<V>> edges) {
        super(nodes, edges);
        assert edges != null;
        this.edges = new HashSet<>(edges);
        this.nodes = new HashSet<>(nodes);
    }

    @Override
    public Set<Edge<V>> getEdges(Node<V> node) {
        Set<Edge<V>> edges = new HashSet<>();
        for (Edge<V> edge : this.edges){
            if (edge.getAllNodes().contains(node)){
                edges.add(edge);
            }
        }
        return edges;
    }

    @Override
    public Node<V> getNodeByData(V data) throws NodeDataNotExsistsException {
        for (Node<V> node : this.nodes){
            if (node.getData().equals(data)){
                return node;
            }
        }
        throw new NodeDataNotExsistsException(data);
    }

    @Override
    public Graph<V> addNodes(Set<Node<V>> nodes) {
        Set<Node<V>> newNodes = new HashSet<>(this.nodes);
        newNodes.addAll(nodes);
        return new DefaultGraph<>(newNodes, edges);
    }

    @Override
    public Graph<V> addEdges(Set<Edge<V>> edges) {
        Set<Edge<V>> newEdges = new HashSet<>(this.edges);
        newEdges.addAll(edges);
        return new DefaultGraph<>(nodes, newEdges);
    }

    @Override
    public Graph<V> removeNodes(Set<Node<V>> nodes) {
        Set<Node<V>> newNodes = new HashSet<>(this.nodes);
        Set<Edge<V>> edges = new HashSet<>(this.edges);

        for (Node<V> node : nodes){
            edges.removeIf(edge -> edge.getAllNodes().contains(node));
        }

        newNodes.removeAll(nodes);
        return new DefaultGraph<>(newNodes, edges);
    }

    @Override
    public Graph<V> removeEdges(Set<Edge<V>> edges) {
        Set<Edge<V>> newEdges = new HashSet<>(this.edges);
        newEdges.removeAll(edges);
        return new DefaultGraph<>(nodes, newEdges);
    }

    @Override
    public Set<Edge<V>> getAllEdges() {
        return new HashSet<>(edges);
    }

    @Override
    public Set<Node<V>> getAllNodes() {
        return new HashSet<>(nodes);
    }
}
