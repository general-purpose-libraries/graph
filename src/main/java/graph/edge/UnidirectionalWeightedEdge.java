package graph.edge;

import graph.node.DefaultNode;
import graph.node.Node;

import java.math.BigDecimal;

/**
 * This class represents a weighted edge which can be travelled only in one direction
 */
public class UnidirectionalWeightedEdge<V> extends UnidirectionalEdge<V> implements WeightedEdge<V> {
    private final BigDecimal weight;

    public UnidirectionalWeightedEdge(Node<V> source, Node<V> target, BigDecimal weight) {
        super(source, target);
        assert weight != null;
        this.weight = weight;
    }

    @Override
    public BigDecimal getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        UnidirectionalWeightedEdge<?> that = (UnidirectionalWeightedEdge<?>) o;

        return weight != null ? weight.equals(that.weight) : that.weight == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        return result;
    }
}
