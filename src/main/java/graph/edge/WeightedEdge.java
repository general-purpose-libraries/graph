package graph.edge;

import java.math.BigDecimal;

/**
 * This interface represents weighted Edges
 */
public interface WeightedEdge<V> extends Edge<V> {
    /**
     * @return weight of this edge
     */
    BigDecimal getWeight();
}
