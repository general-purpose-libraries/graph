package graph.edge;

import graph.node.Node;
import graph.exception.edge.DestinationNotExistsException;
import graph.exception.edge.SourceNotExistsException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * This class represents an edge which can be travelled only in one direction
 */
public class UnidirectionalEdge<V> implements Edge<V> {
    private final Node<V> source;
    private final Node<V> target;

    public UnidirectionalEdge(Node<V> source, Node<V> target) {
        assert source != null;
        assert target != null;
        assert !source.equals(target);
        this.source = source;
        this.target = target;
    }

    @Override
    public Node<V> getSource(Node<V> target) throws SourceNotExistsException{
        assert target != null;
        if (!this.target.equals(target)){
            throw new SourceNotExistsException(target);
        }
        return source;
    }

    @Override
    public Node<V> getTarget(Node<V> source) throws DestinationNotExistsException{
        assert source != null;
        if (!this.source.equals(source)){
            throw new DestinationNotExistsException(source);
        }
        return target;
    }

    @Override
    public Set<Node<V>> getAllNodes() {
        return new HashSet<>(Arrays.asList(
                source,
                target
        ));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnidirectionalEdge<?> that = (UnidirectionalEdge<?>) o;

        if (source != null ? !source.equals(that.source) : that.source != null) return false;
        return target != null ? target.equals(that.target) : that.target == null;
    }

    @Override
    public int hashCode() {
        int result = source != null ? source.hashCode() : 0;
        result = 31 * result + (target != null ? target.hashCode() : 0);
        return result;
    }
}
