package graph.edge;

import graph.node.Node;

import java.math.BigDecimal;

/**
 * This class represents a weighted edge where a node points to itself
 */
public class CircularWeightedEdge<V> extends CircularEdge<V> implements WeightedEdge<V> {
    private final BigDecimal weight;
    public CircularWeightedEdge(Node<V> node, BigDecimal weight) {
        super(node);
        assert weight != null;
        this.weight = weight;
    }

    @Override
    public BigDecimal getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CircularWeightedEdge<?> that = (CircularWeightedEdge<?>) o;

        return weight != null ? weight.equals(that.weight) : that.weight == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        return result;
    }
}
