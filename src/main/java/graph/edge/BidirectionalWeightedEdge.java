package graph.edge;

import graph.node.Node;

import java.math.BigDecimal;

/**
 * This class represents a weighted edge which can be travelled in both directions.
 */
public class BidirectionalWeightedEdge<V> extends BidirectionalEdge<V> implements WeightedEdge<V> {
    private final BigDecimal weight;

    public BidirectionalWeightedEdge(Node<V> node1, Node<V> node2, BigDecimal weight) {
        super(node1, node2);
        assert weight != null;
        this.weight = weight;
    }

    @Override
    public BigDecimal getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BidirectionalWeightedEdge<?> that = (BidirectionalWeightedEdge<?>) o;

        return weight != null ? weight.equals(that.weight) : that.weight == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        return result;
    }
}
