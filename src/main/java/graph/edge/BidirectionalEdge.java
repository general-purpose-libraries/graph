package graph.edge;

import graph.exception.edge.EdgeException;
import graph.exception.edge.NodeNotExistsException;
import graph.node.Node;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * This class represents an edge which can be travelled in both directions.
 */
public class BidirectionalEdge<V> implements Edge<V>{
    private final Node<V> node1;
    private final Node<V> node2;

    public BidirectionalEdge(Node<V> node1, Node<V> node2) {
        assert node1 != null;
        assert node2 != null;
        assert !node1.equals(node2);
        this.node1 = node1;
        this.node2 = node2;
    }

    @Override
    public Node<V> getSource(Node<V> node) throws EdgeException {
        assert node != null;
        return getTarget(node);
    }

    @Override
    public Node<V> getTarget(Node<V> node) throws EdgeException {
        assert node != null;
        if (node.equals(node1)){
            return node2;
        }
        if (node.equals(node2)){
            return node1;
        }
        throw new NodeNotExistsException(node);
    }

    @Override
    public Set<Node<V>> getAllNodes() {
        return new HashSet<>(Arrays.asList(
                node1,
                node2
        ));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BidirectionalEdge<?> that = (BidirectionalEdge<?>) o;

        if (node1 != null ? !node1.equals(that.node1) : that.node1 != null) return false;
        return node2 != null ? node2.equals(that.node2) : that.node2 == null;
    }

    @Override
    public int hashCode() {
        int result = node1 != null ? node1.hashCode() : 0;
        result = 31 * result + (node2 != null ? node2.hashCode() : 0);
        return result;
    }
}
