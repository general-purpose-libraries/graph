package graph.edge;

import graph.node.Node;
import graph.exception.edge.EdgeException;
import graph.exception.edge.NodeNotExistsException;

import java.util.Collections;
import java.util.Set;

/**
 * This class represents an edge where a node points to itself
 */
public class CircularEdge<V> implements Edge<V> {
    private final Node<V> node;

    public CircularEdge(Node<V> node) {
        this.node = node;
    }

    @Override
    public Node<V> getSource(Node<V> node) throws EdgeException {
        assert node != null;
        return getTarget(node);
    }

    @Override
    public Node<V> getTarget(Node<V> node) throws EdgeException {
        assert node != null;
        if (!this.node.equals(node)){
            throw new NodeNotExistsException(node);
        }
        return node;
    }

    @Override
    public Set<Node<V>> getAllNodes() {
        return Collections.singleton(node);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CircularEdge<?> that = (CircularEdge<?>) o;

        return node != null ? node.equals(that.node) : that.node == null;
    }

    @Override
    public int hashCode() {
        return node != null ? node.hashCode() : 0;
    }
}
