package graph.edge;

import graph.exception.edge.EdgeException;
import graph.node.Node;

import java.util.Set;

/**
 * This interface represents edges of a graph
 * @param <V> Type of data stored in the nodes of the graph
 */
public interface Edge<V> {
    /**
     * @param node Node to find source for
     * @return Source node of the given node
     * @throws EdgeException If an error occurs
     */
    Node<V> getSource(Node<V> node) throws EdgeException;

    /**
     * @param node Node to find target for
     * @return target node of the given node
     * @throws EdgeException If an error occurs
     */
    Node<V> getTarget(Node<V> node) throws EdgeException;

    /**
     * @return All nodes of this edge
     */
    Set<Node<V>> getAllNodes();
}
